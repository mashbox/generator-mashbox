# Mashbox Boilerplate Generator #

This is a Yeoman generator for Mashbox projects, this will create the boilerplate file structure with tools used for any project.

### Requirements ###

-Node
-Yeoman

### Installation ###
Once you clone this repository and have the requirements installed, you will need to browse to the 'generator-mashbox' directory and type:

```
$> npm link

```
This will create the npm package that you can reference for new projects.

### Creating a new project ###

From the terminal, create a directory for you new project and open it. To create the boilerplate type:

```
$> yo mashbox

```

This will create the files and install any dependencies
