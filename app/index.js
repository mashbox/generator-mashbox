var generators = require('yeoman-generator');
var util = require('util');
var path = require('path');

var slugify = function (name) {
  return name.replace(/[!\"#$%&'\(\)\*\+,\.\/:;<=>\?\@\[\\\]\^`\{\|\}~]/g, '').replace(/\s+/g, '-').toLowerCase();
};

module.exports = generators.Base.extend({
  constructor: function () {
    // Calling the super constructor is important so our generator is correctly set up
    generators.Base.apply(this, arguments);
  },
  promptUser: function() {
    var done = this.async();
    var prompts = [];

    // have Yeoman greet the user
    this.log(this.yeoman);

    prompts = [{
      name: 'name',
      message: 'What is your project\'s name ?'
    }];

    this.prompt(prompts, function (props) {
        this.name = props.name;

        done();
    }.bind(this));
  },
  copyFiles: function () {
    this.log('Generating boilerplate files...');

    var context = {
      name: this.name,
      slug: slugify(this.name)
    };

    this.directory('src', 'src');
    this.directory('gulp', 'gulp');
    this.directory('test', 'test');
    this.copy('gulpfile.js', 'gulpfile.js');
    this.template('package.json', 'package.json', context);
    this.template('bower.json', 'bower.json', context);
    this.copy('README.md', 'README.md');
    this.copy('.gitignore', '.gitignore');
    this.copy('.eslintrc', '.eslintrc');
  },
  installDeps: function () {
    this.log('Install dependencies...');
    this.installDependencies();
  },
  completed: function () {
    this.log('Woohoo! done.');
  }
});
