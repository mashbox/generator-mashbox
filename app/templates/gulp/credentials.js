module.exports = {
  aws: {
    params: {
      Bucket: "", // The Bucket Name: 'clarify.mashbox.com'
    },
    path: "", // The Subdirectory: 'jigsaw'
    distributionId: "" // The Cloudfront distro: 'E3E6MM9RK82DN1'
  }
}