var gulp   = require('gulp');
var mochaPhantomjs = require('gulp-mocha-phantomjs');

gulp.task('test', ['browserify'], function() {
  return gulp.src('test/index.html')
    .pipe(mochaPhantomjs());
});
