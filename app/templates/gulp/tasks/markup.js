var gulp = require('gulp');
var config = require('../config')
var handleErrors = require('../util/handleErrors');

gulp.task('markup', function() {
  return gulp.src(config.env.src + config.markup.path + config.markup.files)
    .on('error', handleErrors)
    .pipe(gulp.dest(config.env.tmp));
});
