var gulp   = require('gulp');
var RevAll = require('gulp-rev-all');
var config = require('../config');
var handleErrors = require('../util/handleErrors');

gulp.task('rev', function() {
  var revAll = new RevAll({ dontRenameFile: [/.*.ico$/g, /.*.html$/g, /.*.txt$/g] });

  return gulp.src(config.env.tmp + '/**/*')
    .on('error', handleErrors)
    .pipe(revAll.revision())
    .pipe(gulp.dest(config.env.dst))
    .pipe(revAll.manifestFile())
    .pipe(gulp.dest(config.env.dst));
});
