var gulp     = require('gulp');
var imagemin = require('gulp-imagemin');
var pngquant = require('imagemin-pngquant');
var config   = require('../config');
var handleErrors = require('../util/handleErrors');

gulp.task('images', function() {
  return gulp.src(config.env.src + config.images.path + config.images.files)
    .on('error', handleErrors)
    .pipe(imagemin({
      progressive: true,
      svgoPlugins: [{removeViewBox: false}],
      use: [pngquant()]
    })) // Optimize
    .pipe(gulp.dest(config.env.tmp + config.images.path));
});