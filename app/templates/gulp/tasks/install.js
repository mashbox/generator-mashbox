var gulp    = require('gulp');
var install = require("gulp-install");
var handleErrors = require('../util/handleErrors');

gulp.task('install', function() {
  return gulp.src(['./bower.json', './package.json'])
    .on('error', handleErrors)
    .pipe(install());
});
