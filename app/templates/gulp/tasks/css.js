var gulp   = require('gulp');
var config = require('../config');
var handleErrors = require('../util/handleErrors');

gulp.task('css', function() {
  return gulp.src(config.env.src + config.css.path + config.css.files)
    .on('error', handleErrors)
    .pipe(gulp.dest(config.env.tmp + config.css.path));
});
