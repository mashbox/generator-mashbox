// automaticall install packages package.json or bower.json
var gulp = require('gulp');
var runSequence = require('run-sequence');

gulp.task('default', function () {
  runSequence('setWatch', ['images', 'fonts', 'css', 'markup'], 'sass', ['browserify'], 'browserSync', 'watch');
});
