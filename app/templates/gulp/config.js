var dst = './build';
var src = './src';
var tmp = './tmp';

// TODO: Steve -- change this. Instead of having one config file for all tasks. Move the config for each task to its task file.

module.exports = {
  // Environment Paths
  env: {
    dst: dst,
    src: src,
    tmp: tmp
  },

  // Browser Sync
  browserSync: {
    server: {
      // We're serving the src folder as well
      // for sass sourcemap linking
      baseDir: [tmp]
    },
    files: [
      tmp + '/**',
      // Exclude Map files
      '!' + tmp + '/**.map'
    ]
  },

  // Browserify
  browserify: {
    // Add in some aliases
    aliases: {
    },
    // A separate bundle will be generated for each
    // bundle config in the list below
    bundleConfigs: [{
      entries: src + '/javascript/main.js',
      dest: tmp + '/js',
      outputName: 'main.js',
      // Enable source maps
      debug: false,
      // Additional file extentions to make optional
      extensions: ['jsx'],
      external: [
      ]
    }, {
      dest: tmp + '/js',
      outputName: 'vendor.js',
      // Enable source maps
      debug: false,
      require: [
      ]
    }, {
      entries: './test/index.js',
      dest: tmp + '/js',
      outputName: 'client-test.js',
      external: []
    }]
  },

  // SASS
  sass: {
    path: '/sass',
    files: '/**/*.{scss,sass}',
    settings: {
      // Required if you want to use SASS syntax
      // See https://github.com/dlmanning/gulp-sass/issues/81
      sourceComments: 'map',
      imagePath: '/img' // Used by the image-url helper
    }
  },

  // CSS
  css: {
    path: '/css',
    files: '/**/*.css'
  },

  // JS
  js: {
    path: '/',
    files: '/**/*.js'
  },

  // Fonts
  fonts: {
    path: '/fonts',
    files: '/**/*.{eot,woff,ttf,svg}'
  },

  // Images
  images: {
    path: '/img',
    files: '/**/*'
  },

  // Markup
  markup: {
    path: '',
    files: '/*.{html,txt,ico}'
  }
};
